package com.safebear.auto.pages.locators;

import lombok.Data;
import lombok.Getter;
import org.openqa.selenium.By;

@Getter
@Data
public class LoginPageLocators {

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.name("psw");
    private By loginButtonLocator = By.id("enter");


    private By validationMessage = By.xpath(".//p[@id='rejectLogin']/b");


private By rememberMeCheckBox = By.xpath(".//input[@name='remember'']");




}
