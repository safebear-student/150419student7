package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {
    @Test
    public void testEmployee(){

        //This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

        //this is where we employee hannah and fire bob
        hannah.employ();
        bob.fire();

        victoria.employ();
        victoria.changeCar("bmw");

        //lets print out their state to screen
        System.out.println("Hannah employement state: " + hannah.isEmployed());
        System.out.println("Bob employement state: " + bob.isEmployed());
        System.out.println ("Victoria employement state: " + victoria.isEmployed());
        System.out.println("Victoria's car: " + victoria.car);


    }






}
